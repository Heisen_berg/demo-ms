package org.sid.demoms;

import org.sid.demoms.entities.Client;
import org.sid.demoms.repositories.ClientRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class DemoMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMsApplication.class, args);
    }

    @Bean
    CommandLineRunner start(ClientRepository clientRepository) {
        return args -> {
            clientRepository.save(new Client(1L, "ysf", "dfli", "dfli@email.com", 23));
            clientRepository.save(new Client(2L, "hmd", "Etrz", "etrz@email.com", 20));
            clientRepository.save(new Client(3L, "abdeer", "daou", "daou@email.com", 20));
        };
    }
}
