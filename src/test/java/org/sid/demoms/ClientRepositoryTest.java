package org.sid.demoms;

import org.junit.jupiter.api.Test;
import org.sid.demoms.entities.Client;
import org.sid.demoms.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ClientRepositoryTest {

    @Autowired
    private ClientRepository clientRepository;

    @Test
    void injected_elements_should_not_be_null(){
        clientRepository.save(new Client(555L, "DAFALI", "Youssef",
                "dafaliysf@gmail.com", 22));
        assertThat(clientRepository).isNotNull();
    }

}
